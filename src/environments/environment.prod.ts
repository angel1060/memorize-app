export const environment = {
  production: true,
  backendUrl: 'http://memorize.us-east-2.elasticbeanstalk.com/',
  frontendUrl: 'http://sig.web.s3-website.us-east-2.amazonaws.com/#/'
};
