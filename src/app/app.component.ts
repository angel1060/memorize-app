import { Component, OnInit } from '@angular/core';
import { Memorize } from './model/memorize';
import { Sentence } from './model/sentence';
import Swal, { SweetAlertType } from 'sweetalert2';
import { MemorizeService } from './service/memorize.service';
import { VIEW_MODE } from './util/enums';
import { Alternative } from './model/alternative';
// https://stackoverflow.com/questions/10721884/render-html-to-an-image
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
    title = 'memorize-app';

    constructor() { }

    ngOnInit() {
    }
}
