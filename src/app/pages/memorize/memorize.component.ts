import { Component, OnInit, ViewChild, ElementRef, HostListener, Renderer2, AfterViewChecked } from '@angular/core';
import { Memorize } from '../../model/memorize';
import Swal, { SweetAlertType } from 'sweetalert2';
import { MemorizeService } from '../../service/memorize.service';
import { VIEW_MODE } from '../../util/enums';
import { Alternative } from '../../model/alternative';
import { ActivatedRoute } from '@angular/router';
import { MemorizeFilter } from 'src/app/model/MemorizeFilter';
import { trimTrailingNulls } from '@angular/compiler/src/render3/view/util';
import { log } from 'util';
import { error } from '@angular/compiler/src/util';
import { SummaryTotalProgress } from '../../model/SummaryTotalProgress';
declare var $: any;

@Component({
  templateUrl: './memorize.component.html',
  styleUrls: ['./memorize.component.css']
})
export class MemorizeComponent implements OnInit, AfterViewChecked {

  

    title = 'memorize-app';
    // memorize: Memorize = new Memorize();
    actual_memorize: Memorize;
    manage_object_memorize: Memorize;
    memorizes: Memorize[] = [];
    memorizesCompleted: Memorize[] = [];
    index: number = 0;
    input: string = "";
    newEnglishAlternative: string = "";
    advance_percentage: number = 0;
    viewMode: any = VIEW_MODE.LEARNING;
    showAnswer: boolean = false;
    isLoading: boolean = false;
    memorizeFilter: MemorizeFilter = new MemorizeFilter();
    audioAddress: string;
    isMute: boolean = false;
    onlyImg: boolean = false;
    summaryTotalProgress: SummaryTotalProgress = new SummaryTotalProgress();

    // Variables para carga de datos inicial
    // limit: number = null;
    // minor100Percent: boolean = true;
    // typeWords: number = 1;

    public ngAfterViewChecked(): void {
        // $("#input_english").focus();
        // document.getElementById("input_english").focus();
    }

    constructor(private memorizeService: MemorizeService,
                public renderer: Renderer2,
                private route: ActivatedRoute) { 

                }
         
    ngOnInit() {
      this.checkParams();
      this.initObjects();
      this.getListMemorize(1);
      this.loadSummaryTotalProgress();

      // console.log("localStorage.getItem(onlyImg): "+localStorage.getItem("onlyImg"));
      if(!localStorage.getItem("onlyImg")){
        console.log("ingresó a crear localstorage");
          localStorage.setItem("onlyImg", "true");
      }else{
        this.onlyImg = Boolean(localStorage.getItem("onlyImg"));
      }
      
    }

    initObjects(){
        this.actual_memorize = new Memorize();
    }


    loadSummaryTotalProgress(){
        // retrieveSummaryTotalProgress
        const myObservable  = this.memorizeService.retrieveSummaryTotalProgress().subscribe(
          (data) => {
            this.summaryTotalProgress = data;
          },

          (error) => {
            console.error(error);
          },

          () => {}   
        );
    }


    checkParams(){
        const sub: any = this.route
        .queryParams
        .subscribe(params => {
            this.memorizeFilter.limit = params['limit'];
            this.memorizeFilter.minor100 = params['onlyMinor100'];
            this.memorizeFilter.typeWord = params['typeWord'];
            this.memorizeFilter.sortMinor100 = params['sortMinor100'];
        });
    }



    getListMemorize(typeId: number){
        this.isLoading = true;

        const myObservable  = this.memorizeService.findLisByFilter(this.memorizeFilter).subscribe(
          (data) => {
            this.memorizes = data;
          },

          (error) => {
            console.error(error);
          },

          () => {// al terminar la notificación
            this.refreshActualMemorize();
            this.isLoading = false;
            
            // this.renderer.selectRootElement('#input_english').focus();
            // try {
              // document.getElementById('input_english').focus();
            // } catch (error) {}
          }   
        );
    }

    refreshActualMemorize(){
      // enviamos frases al azar    
      
      if(this.memorizes.length > 0 ){
          // this.index = this.getRandom(0, this.memorizes.length - 1);
          // this.actual_memorize = this.memorizes[this.index];
          
          this.actual_memorize = this.memorizes[0];
          this.audioAddress = "http://translate.google.com/translate_tts?tl=en&q="+this.actual_memorize.english.trim()+"&client=tw-ob";
          this.calculateAvancePercentage();
      }else{
        // Cuando las frases principales hayan acabado reiniciamos el ciclo y guardamos estados en base de datos
        this.updateMemorizesStatus();
      }

      
    }


    onKeydown(event) { 
      if(event.key === "|"){
        this.playAudio();
        return false;
      }

      if(event.key === "F1"){
        this.playAudio(this.input.trim());
        return false;
      }

      if (event.key === "Enter") {
        if(this.validateSentence()){
          if(!this.isMute){
            this.playAudio();
          }
          
          this.input = "";
          this.showAnswer = false;
          this.actual_memorize.isApproved = true;
          this.actual_memorize.scor++;

          
          this.memorizesCompleted.push(this.actual_memorize);
          // this.memorizes.splice(this.index, 1);// Eliminamos objeto que ya respondimos
          this.memorizes.splice(0, 1);// Eliminamos objeto que ya respondimos
          
          this.calculateAvancePercentage();
          this.refreshActualMemorize();
          this.showMessage(true);
        }else{
          this.showMessage(false);
          this.addFault();
        }
        
      }
    }



    validateSentence(): boolean{
        const input_temp = this.removeQuestionMark(this.input.trim().toLowerCase());     

        if(input_temp === this.removeQuestionMark(this.actual_memorize.english.trim().toLowerCase())){
            return true;
        }else{
          for (const alternative of this.actual_memorize.alternatives) {
              if(input_temp === this.removeQuestionMark(alternative.english.trim().toLowerCase())){
                  return true;
              }
          }
          return false;
        }
    }


    calculateAvancePercentage(){
        // Porcentaje avance por sessión
        this.advance_percentage = this.memorizesCompleted.length * 100 / (this.memorizesCompleted.length + this.memorizes.length);        
        this.updatePercentage(this.advance_percentage);

        // Porcentaje avance progreso memorización de la palabra
        if(this.actual_memorize.scor <= this.actual_memorize.requiredRepetitions){
            // this.actual_memorize.advancePercentage = Number(Number.parseFloat(String(this.actual_memorize.scor * 100 / this.actual_memorize.requiredRepetitions)).toFixed(0));
            this.actual_memorize.advancePercentage = Math.round(this.actual_memorize.scor * 100 / this.actual_memorize.requiredRepetitions);
            
        }else{
            this.actual_memorize.advancePercentage = 100;
        }
        
    }

    removeQuestionMark(str: string): string{
       return str.replace('?', '').replace('¿', '');
    }

    getRandom(min, max) {
      return Math.floor(Math.random() * (max - min)) + min;
    }


    showMessage(approved: boolean, message?: string){     
      let type_: string = 'success';
      let title: string = 'approved';

      if(!approved){
        type_ = 'error';
        title = 'wrong';
      }

      const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000
      });

      Toast.fire({
        type: <SweetAlertType>type_,
        title: !message ? title : message 
      });
    }

    updatePercentage(current_progress: number){
        document.getElementById('progress_bar').style.width = current_progress+'%';  
    }


    manageDictionary(){
       this.viewMode = 'MANAGE_DICTIONAY';
       // this.manage_object_memorize = this.actual_memorize;
       this.manage_object_memorize = new Memorize();
    }

    search(){
      this.memorizeService.likeFirst(this.manage_object_memorize).subscribe(
        (response) => { 
          this.manage_object_memorize = response;
        }
      );
    }

    save(){
        this.memorizeService.saveMemorize(this.manage_object_memorize).subscribe(
          (response) => {  
            this.showMessage(response);
            this.manage_object_memorize = new Memorize();
          }
        );
    }

    addAlternative(){
        if(this.newEnglishAlternative.trim() !== ''){
          const altternative: Alternative = new Alternative();
          // altternative.memorize = this.manage_object_memorize;
          altternative.english = this.newEnglishAlternative;
  
          if(!this.manage_object_memorize.alternatives){
              this.manage_object_memorize.alternatives = [];
          }
  
          this.manage_object_memorize.alternatives.push(altternative);
          this.newEnglishAlternative = "";
        }
    }

    showHelp(){
        this.showAnswer = true;
        this.actual_memorize.helps++;

        // Cada vez que se pida una ayuda aumentamos las cantidad de repeticiones necesarias
        this.actual_memorize.requiredRepetitions++;
    }

    addFault(){
      this.actual_memorize.faults++;
    }

    clean(){
      this.manage_object_memorize = new Memorize();
    }

    updateMemorizesStatus(){
        this.isLoading = true;

        this.memorizeService.updateMemorizesStatus(this.memorizesCompleted).subscribe(
          (response) => { 
             this.showMessage(response, 'Updated progress!');             
          },
          (error) => {
              console.log(error);
          },
          () => {
            // actualizamos lista con siguientes palabras
            this.getListMemorize(1);
            this.memorizesCompleted = [];
            this.calculateAvancePercentage();
            this.loadSummaryTotalProgress();
          }
        );
    }


    @HostListener('document:keydown', ['$event']) onKeydownHandler(event: KeyboardEvent) { 
      if (event.key === "Escape") { 
          this.showHelp();
      } 
    }
    
    

    test(){
     
    }


    playAudio(sentence_?:string){
      const sentence: string = sentence_ ? sentence_ : this.actual_memorize.english.trim();
      const msg = new SpeechSynthesisUtterance(sentence);
      msg.lang = 'en-US';
      
      window.speechSynthesis.speak(msg);

      // const msg = new SpeechSynthesisUtterance();
      // const voices = window.speechSynthesis.getVoices();
      // console.log(sentence);
      // msg.voice = voices[6];// Voz hombre [2] y [6 UK Hombre]..   [4](mujer USD English)
      // msg.voiceURI = "native";
      // msg.volume = 1;
      // msg.rate = 1;      
      // msg.pitch = 0.8;
      // msg.text = sentence;
      // msg.lang = 'en';
      // speechSynthesis.speak(msg);

    }


    playAudio_x(sentence_?:string){
      // const sentence = this.actual_memorize.english.trim().replace(/ /g, '+');
      const sentence = sentence_ ? sentence_ : this.actual_memorize.english.trim();
      
      const audio = new Audio();
      audio.src = "http://translate.google.com.vn/translate_tts?ie=UTF-8&q="+sentence+"&tl=en&client=tw-ob";
      audio.load();
      const playPromise = audio.play();
      
      if (playPromise !== undefined) {
        playPromise.then(_ => { console.log("funcionó1");})
        .catch(error => {
            audio.src = "http://translate.google.com.vn/translate_tts?ie=UTF-8&client=tw-ob&q="+sentence+"&tl=en";
            audio.load();
            const playPromise2 = audio.play();
            
            if (playPromise2 !== undefined) {
              playPromise2.then(_ => {console.log("funcionó2");})
              .catch(error => {
                audio.src = "http://translate.google.com.vn/translate_tts?ie=UTF-8&q="+sentence+"&client=tw-ob&tl=en";
                audio.load();
                const playPromise3 = audio.play(); 

                if (playPromise3 !== undefined) {
                  playPromise3.then(_ => {console.log("funcionó3");})
                  .catch(error => {
                        audio.src = "http://translate.google.com.vn/translate_tts?ie=UTF-8&client=tw-ob&q="+sentence+"&tl=en";
                        audio.load();
                        const playPromise2 = audio.play();
                        
                        if (playPromise2 !== undefined) {
                          playPromise2.then(_ => {console.log("funcionó2");})
                          .catch(error => {
                                audio.src = "http://translate.google.com.vn/translate_tts?ie=UTF-8&client=tw-ob&tl=en&q="+sentence;
                                audio.load();
                                const playPromise3 = audio.play(); 
                
                                if (playPromise3 !== undefined) {
                                  playPromise3.then(_ => {console.log("funcionó4");})
                                  .catch(error => {
                                        audio.src = "http://translate.google.com.vn/translate_tts?client=tw-ob&q="+sentence+"&tl=en";
                                        audio.load();
                                        const playPromise3 = audio.play(); 
                        
                                        if (playPromise3 !== undefined) {
                                          playPromise3.then(_ => {console.log("funcionó5");})
                                          .catch(error => {
                                                  audio.src = "http://translate.google.com.vn/translate_tts?ie=UTF-8&tl=en&client=tw-ob&q="+sentence;
                                                  audio.load();
                                                  const playPromise3 = audio.play(); 
                                  
                                                  if (playPromise3 !== undefined) {
                                                    playPromise3.then(_ => {console.log("funcionó6");})
                                                    .catch(error => {// probar este para todos
                                                              audio.src = "http://translate.google.com.vn/translate_tts?client=tw-ob&q="+sentence+"&tl=en&ie=UTF-8";
                                                              audio.load();
                                                              const playPromise3 = audio.play(); 
                                              
                                                              if (playPromise3 !== undefined) {
                                                                playPromise3.then(_ => {console.log("funcionó7");})
                                                                .catch(error => {
                                                                      audio.src = "http://translate.google.com.vn/translate_tts?ie=UTF-8&q="+sentence.trim().replace(/ /g, '+')+"&tl=en&client=tw-ob";
                                                                      audio.load();
                                                                      const playPromise3 = audio.play(); 
                                                      
                                                                      if (playPromise3 !== undefined) {
                                                                            playPromise3.then(_ => {console.log("funcionó9");})
                                                                            .catch(error => {
                                                                              playPromise3.then(_ => {console.log("funcionó7");})
                                                                              .catch(error => {
                                                                                    audio.src = "http://translate.google.com/translate_tts?tl=en&q="+sentence.trim()+"&client=tw-ob";
                                                                                    audio.load();
                                                                                    const playPromise3 = audio.play(); 
                                                                    
                                                                                    if (playPromise3 !== undefined) {
                                                                                      playPromise3.then(_ => {console.log("funcionó8");})
                                                                                      .catch(error => {
                                                                                          // error
                                                                                      });
                                                                                    }
                                                                              });
                                                                        });
                                                                      }
                                                                });
                                                              }
                                                    });
                                                  }
                                          });
                                        }
                                  });
                                }
                          });
                        }
                  });
                }
              });
            }
        });
      }
    }

    setOnlyImage(){
      this.onlyImg = !this.onlyImg;
      localStorage.setItem("onlyImg", String(this.onlyImg));
    }


    // playAudio_xxx(){
    //   const sentence = this.actual_memorize.english.trim();
      
      
    //   const audio = new Audio();
    //   audio.src = "https://translate.google.com/translate_tts?ie=UTF-8&client=tw-ob&q="+sentence+"&tl=en&total=1&idx=0&textlen=5";
    //   audio.load();
    //   const playPromise = audio.play();
      
    //   if (playPromise !== undefined) {
    //     playPromise.then(_ => { console.log("funcionó1");})
    //     .catch(error => {
    //        console.log(error);
    //     });
    //   }
    // }

    onLoadInputSection(){
      console.log("helll");
    }
}
