import {Injectable} from '@angular/core';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Memorize } from '../model/memorize';
import { MemorizeFilter } from '../model/MemorizeFilter';
import { environment } from 'src/environments/environment';
import { SummaryTotalProgress } from '../model/SummaryTotalProgress';

@Injectable()
export class MemorizeService {

    constructor(private http: HttpClient) {}

    // private apiUrl:string = "http://localhost:8080/memorize";
    private apiUrl = `${environment.backendUrl}${'memorize'}`;
    // private apiUrl = `${environment.backendUrl}${'investment'}`;

    private getHeader(): HttpHeaders {
        const httpHeaders = new HttpHeaders({'Authorization': 'Bearer ' + 'eyJhbGciOiJIUzUxMiJ9.eyJhdXRob3JpdGllcyI6Ilt7XCJhdXRob3JpdHlcIjpcIlJPTEVfRExMT1wifV0iLCJzdWIiOiIxIiwiaWF0IjoxNTUzNTY3ODk5LCJleHAiOjE1NTM1ODE4OTl9.oh8HiMEfac-MFLj5qe0jZCKQU5LUGdDAOJOUemK4UFOXWxw98nMLdVnRroL6AWqOZmMwpqX4iUXRqU39oPzpyA', 'Content-type': 'application/json'});
        return httpHeaders;
    }


    getListMemorize(typeId:number): Observable<Memorize[]>{
         return this.http.get(`${this.apiUrl+"/getListMemorize"}/${typeId}`).pipe(
         map( (response) => response as Memorize[])
       );
    }

//     findListByIdTypeAndAdvancePercentageMinor100(typeId:number): Observable<Memorize[]>{
//         return this.http.get(`${this.apiUrl+"/findLisByFilter"}/${typeId}`).pipe(
//         map( (response) => response as Memorize[])
//       );
//    }

   findLisByFilter(filter: MemorizeFilter): Observable<Memorize[]>{
        return this.http.post<Memorize[]>(this.apiUrl+"/findLisByFilter", filter, {headers: this.getHeader()});
    }
    

    saveMemorize(memorize: Memorize): Observable<boolean>{
        return this.http.post<boolean>(this.apiUrl+"/saveMemorize", memorize);
    }

    likeFirst(memorize: Memorize): Observable<Memorize>{
        return this.http.post<Memorize>(this.apiUrl+"/likeFirst", memorize);
    }

    
    updateMemorizesStatus(memorizes: Memorize[]): Observable<boolean>{
        return this.http.post<boolean>(this.apiUrl+"/updateMemorizesStatus", memorizes);
    }

    retrieveSummaryTotalProgress(): Observable<SummaryTotalProgress>{
        return this.http.get(`${this.apiUrl+"/retrieveSummaryTotalProgress"}`).pipe(
        map( (response) => response as SummaryTotalProgress)
      );
   }
   
}
