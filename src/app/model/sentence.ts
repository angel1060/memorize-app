export class Sentence {
    english: string;
    spanish: string;
    strong_level: number;
    isSuccessful: boolean;
}