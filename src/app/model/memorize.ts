import { Sentence } from './sentence';
import { Alternative } from './alternative';

export class Memorize {

    id: number;
    spanish: string;
    english: string;
    example: string;
    typeId: number;
    requiredRepetitions: number;
    scor: number;
    faults: number;
    helps: number;
    imageUrl: any;
    resourcelink: any;
    advancePercentage: number;
    alternatives: Alternative[];

    

    // Temp
    isApproved: boolean = false;

}