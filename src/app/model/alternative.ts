import { Memorize } from "./memorize";

export class Alternative {
    alternativeId: number;
    english: string;
    memorize: Memorize;
}