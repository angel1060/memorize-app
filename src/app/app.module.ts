import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';
import { MemorizeService } from './service/memorize.service';
import { HttpClientModule, HTTP_INTERCEPTORS  } from '@angular/common/http';
import { ToimgComponent } from './pages/toimg/toimg.component';
import { APP_ROUTES } from './app.routes';
import { MemorizeComponent } from './pages/memorize/memorize.component';


@NgModule({
  declarations: [
    AppComponent,
    ToimgComponent,
    MemorizeComponent
  ],
  imports: [
    APP_ROUTES,
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    FormsModule
  ],
  exports: [
    HttpClientModule
  ],
  providers: [
    MemorizeService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
