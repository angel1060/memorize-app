import {Routes, RouterModule} from '@angular/router';
import {ModuleWithProviders} from '@angular/core';
import { MemorizeComponent } from './pages/memorize/memorize.component';
import { ToimgComponent } from './pages/toimg/toimg.component';


export const routes: Routes = [
    { path: '', component:MemorizeComponent },
    { path: 'memorize', component:MemorizeComponent },
    { path: 'toimg', component:ToimgComponent }
];

export const APP_ROUTES: ModuleWithProviders = RouterModule.forRoot(routes, { useHash: true } );
