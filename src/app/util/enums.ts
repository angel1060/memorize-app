export enum MESSAGES_TYPE {
    WARNING = "warning",
    ERROR = "error",
    INFO = "info",
}

export enum VIEW_MODE {
    MANAGE_DICTIONAY = "MANAGE_DICTIONAY",
    LEARNING = "LEARNING",
}

export enum AWS_S3 {
    URL_S3 = "https://s3.amazonaws.com/",
    URL_PROFILE_IMAGES = "https://s3.amazonaws.com/sig.web.storage/profile-picture/",
    STORAGE_PAYMENT_PROFF = "sig.web.storage/payment-proff/",
    STORAGE_PROFILE_PICTURE = "sig.web.storage/profile-picture"
}


